Исключения
===========

Пример

.. literalinclude:: code/exception.php
   :language: php
   :linenos:

Литература

* `<https://www.php.net/manual/ru/language.errors.php7.php>`_
* `<https://www.php.net/manual/ru/spl.exceptions.php>`_
