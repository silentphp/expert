Итераторы
=========

Пример

.. literalinclude:: code/iterator.php
   :language: php
   :linenos:

Литература

* `<https://www.php.net/manual/ru/language.oop5.iterations.php>`_
* `<https://habr.com/ru/post/324934/>`_
* `<https://refactoring.guru/ru/design-patterns/iterator/php/example>`_
* `<https://ruseller.com/lessons.php?rub=37&id=1448>`_
