Магические методы
=================

Пример

.. literalinclude:: code/magic.php
   :language: php
   :linenos:


Литература

`<https://www.php.net/manual/ru/language.oop5.magic.php>`_
