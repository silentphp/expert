Новые версии PHP
===========================

PHP 7.4
--------

`Новое <https://www.php.net/manual/ru/migration74.php>`_

Основные изменения:
    * Типизированный свойства классов
    * Предзагрузка
    * Стрелочные функции
    * Присваивающий оператор объединения с null
    * Разница типов
    * Интерфейс внешней функции
    * Распаковка внутри массивов


Типизированный свойства классов

.. literalinclude:: code/user.php
   :language: php
   :linenos:

Присваивающий оператор объединения с null

    .. code-block:: php

        //Было
        $name = $name ?? "Fedor";
        //Стало
        $name ??= "Fedor";

Интерфейс внешней функции
    Позволяет вызывать в PHP функции и данные C. Позволяет создавать библиотеки на PHP. Класс FFI.

Распаковка внутри массивов

.. code-block:: php

    $arrFirst = [1, 2, 3];
    $arrSecond = [...$arrFirst, 4, 5, 5];
    print_r($arrSecond);




PHP 7.3
--------------

`Новое <https://www.php.net/manual/ru/migration73.php>`_

* Новые глобальные константы

PHP 7.2
--------------

`Новое <https://www.php.net/manual/ru/migration72.php>`_
 отмизация
новая функцуиональность


PHP 7.1
--------------

`Новое <https://www.php.net/manual/ru/migration71.php>`_


PHP 7.0
--------------

`Новое <https://www.php.net/manual/ru/migration70.php>`_

