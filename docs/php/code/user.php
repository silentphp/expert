<?php

class User
{
    public string $name;

    public int $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }
}

$user = new User("Ivan", 22);
var_dump($user);
