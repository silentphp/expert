<?php

class ProductIterator implements Iterator
{
    private $storage = [];

    public function getStorage(): array
    {
        return $this->storage;
    }

    public function setStorage($storage): ProductIterator
    {
        $this->storage[] = $storage;
        return $this;
    }

    public function current()
    {
        return current($this->storage);
    }

    public function next()
    {
        next($this->storage);
    }

    public function key()
    {
        return key($this->storage);
    }

    public function valid()
    {
        return key($this->storage) !== null;
    }

    public function rewind()
    {
        return reset($this->storage);
    }
}

$products = new ProductIterator();
$products->setStorage('Холодильник');
$products->setStorage('Морозильник');
$products->setStorage('Духовка');

foreach ($products as $product) {
    echo $product . PHP_EOL;
}
