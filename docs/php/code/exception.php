<?php

function multiply($a, $b)
{
    if (!is_int($a) || !is_int($b)) {
        throw new InvalidArgumentException("Argument should be integer");
    }
    return $a * $b;
}

echo multiply(12, 5);
echo PHP_EOL;
