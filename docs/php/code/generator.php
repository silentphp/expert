<?php

function generatorArr()
{
    for ($i = 0; $i < 100000; $i++) {
        yield $i;
    }
}

function rangeArr()
{
    $arr = [];
    for ($i = 0; $i < 100000; $i++) {
        $arr[] = $i;
    }
    return $arr;
}

$start = memory_get_usage();
$arr = generatorArr();
//$arr = rangeArr();
foreach ($arr as $item) {
    echo $item . PHP_EOL;
}
echo memory_get_usage() - $start;
echo PHP_EOL;


