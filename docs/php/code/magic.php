<?php

/**
 * @property string userName
 * @property int age
 */
class Model
{

    private $data = [];

    public function __get($name)
    {
        return "Получено: " . $this->data[$name];
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }
}

$model = new Model();
$model->userName = "Ivan";
$model->age = 21;
echo $model->userName . PHP_EOL;
echo $model->age . PHP_EOL;
