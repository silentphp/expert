Генераторы
==========

Работа оператора yield схожа c return, но yield может возвращать значение сколько нужно для генерации значений


.. literalinclude:: code/generator.php
   :language: php
   :linenos:

Потребление памяти при использовании генератора - **416**

Потребление памяти без использовании генератора - **4198512**

Литература

* `<https://www.php.net/manual/ru/language.generators.overview.php>`_
* `<https://habr.com/ru/post/189796/>`_
