Composer
=========

#. Как в composer работает autoload
#. После того как сконфигурировал composer.json для autoload что нужно сделать, чтобы заработало.
#. Что формирует composer после composer dump-autoload
#. Какие функции использует composer для автозагрузки
#. Что такое composer json
#. Как записываются версии
#. Что такое composer lock. Нужно ли его комитить
