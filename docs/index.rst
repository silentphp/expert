PHP Expert
=======================================

Описание
-----------

Проект предназначен для того чтобы собрать все необходимые знания, которыми должен обладать PHP разработчик


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: PHP

   php/new
   php/spl
   php/iterator
   php/generator
   php/exception
   php/magic
   php/psr
   php/composer
   php/unit

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: DB

   db/index
   db/postgresql
   db/trigger
   db/acid
   db/explain

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Теория

   theory/index
   theory/algorithm
   theory/data_structure
   theory/recursion

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Паттерны проектирования

   pattern/index
   pattern/di

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Вспомогательное

   man
