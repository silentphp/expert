Теория
=======

Процессы и потоки
__________________

Процесс это контекст выполнения программы.
Поток это агент в рамках процесса.

Литература
~~~~~~~~~~
* `<https://moodle.kstu.ru/mod/page/view.php?id=49>`_
* `<https://habr.com/ru/post/40227/>`_
* `<https://tproger.ru/problems/what-is-the-difference-between-threads-and-processes/>`_

Принципы SOLID
______________

Литература

`<https://ru.wikipedia.org/wiki/SOLID>`_
