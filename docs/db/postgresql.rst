Установка PostgreSQL
=======

Для mac os через brew

Обновление репозиториев

	.. code-block:: console
    
	    brew update
	    #brew doctor
		
Установка БД

	.. code-block:: console
	
 		brew install postgresql		
		
Настройка 

	.. code-block:: console
	
 		psql postgres
		
		#создание пользователя
		CREATE ROLE newUser WITH LOGIN PASSWORD `password`;
		#назначение роли
		ALTER ROLE newUser CREATEDB;
		#создание БД
		CREATE DATABASE newdb;
		

* `<https://www.sqlshack.com/setting-up-a-postgresql-database-on-mac/>`_
		